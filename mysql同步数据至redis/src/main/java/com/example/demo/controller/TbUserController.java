package com.example.demo.controller;


import com.example.demo.po.TbUser;
import com.example.demo.service.ITbUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class TbUserController {
    @Autowired
    ITbUserService tbUserService;

    @PostMapping("insert")
    public Object insert(@RequestBody TbUser tbUser) {
        tbUserService.save(tbUser);
        return "数据插入完成";
    }

    @PostMapping("update")
    public Object update(@RequestBody TbUser tbUser) {
        tbUserService.updateById(tbUser);
        return "数据更新完成";
    }

    @PostMapping("delete")
    public Object delete(@RequestParam(value = "id") Integer id) {
        tbUserService.removeById(id);
        return "数据删除完成";
    }
}
