package com.example.demo.mapper;

import com.example.demo.po.TbUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author astupidcoder
 * @since 2023-04-06
 */
public interface TbUserMapper extends BaseMapper<TbUser> {

}
