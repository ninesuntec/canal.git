package com.example.demo.client;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.otter.canal.client.CanalConnector;
import com.alibaba.otter.canal.client.CanalConnectors;
import com.alibaba.otter.canal.protocol.CanalEntry;
import com.alibaba.otter.canal.protocol.Message;
import com.example.demo.util.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.net.InetSocketAddress;
import java.util.List;

@Slf4j
@Component
public class CanalClient implements ApplicationRunner {
    @Autowired
    RedisUtils redisUtils;

    private static final String TABLE_NAME = "tb_user";
    private static final String PRIMARY_KEY = "user_id";
    private static final String SEPARATOR = ":";

    private static final String CANAL_SERVER_HOST = "192.168.0.104";
    private static final int CANAL_SERVER_PORT = 11111;
    private static final String CANAL_INSTANCE = "redis";
    private static final String USERNAME = "canal";
    private static final String PASSWORD = "canal";

    @Override
    public void run(ApplicationArguments args) throws Exception {
        this.initCanal();
    }

    public void initCanal() {
        // 创建链接
        CanalConnector connector = CanalConnectors.newSingleConnector(
                new InetSocketAddress(CANAL_SERVER_HOST, CANAL_SERVER_PORT),
                CANAL_INSTANCE, USERNAME, PASSWORD);
        int batchSize = 1000;
        try {
            log.info("启动 canal 数据同步...");
            connector.connect();
            connector.subscribe(".*\\..*");
            connector.rollback();
            while (true) {
                // 获取指定数量的数据
                Message message = connector.getWithoutAck(batchSize);
                long batchId = message.getId();
                int size = message.getEntries().size();
                if (batchId == -1 || size == 0) {
                    try {
                        // 时间间隔1000毫秒
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    syncEntry(message.getEntries());
                }
                connector.ack(batchId); // 提交确认
                // connector.rollback(batchId); // 处理失败, 回滚数据
            }
        } finally {
            connector.disconnect();
        }
    }

    private void syncEntry(List<CanalEntry.Entry> entrys) {
        for (CanalEntry.Entry entry : entrys) {
            if (entry.getEntryType() == CanalEntry.EntryType.TRANSACTIONBEGIN
                    || entry.getEntryType() == CanalEntry.EntryType.TRANSACTIONEND) {
                continue;
            }

            CanalEntry.RowChange rowChange;
            try {
                rowChange = CanalEntry.RowChange.parseFrom(entry.getStoreValue());
            } catch (Exception e) {
                throw new RuntimeException("ERROR data:" + entry.toString(), e);
            }

            CanalEntry.EventType eventType = rowChange.getEventType();
            log.info("================> binlog[{}:{}] , name[{},{}] , eventType : {}",
                    entry.getHeader().getLogfileName(), entry.getHeader().getLogfileOffset(),
                    entry.getHeader().getSchemaName(), entry.getHeader().getTableName(),
                    eventType);

            String tableName = entry.getHeader().getTableName();
            if (!TABLE_NAME.equalsIgnoreCase(tableName)) continue;
            for (CanalEntry.RowData rowData : rowChange.getRowDatasList()) {
                if (eventType == CanalEntry.EventType.INSERT) {
                    printColumn(rowData.getAfterColumnsList());
                    redisInsert(tableName, rowData.getAfterColumnsList());
                } else if (eventType == CanalEntry.EventType.UPDATE) {
                    printColumn(rowData.getAfterColumnsList());
                    redisUpdate(tableName, rowData.getAfterColumnsList());
                } else if (eventType == CanalEntry.EventType.DELETE) {
                    printColumn(rowData.getBeforeColumnsList());
                    redisDelete(tableName, rowData.getBeforeColumnsList());
                }
            }
        }
    }

    private void redisInsert(String tableName, List<CanalEntry.Column> columns) {
        JSONObject json = new JSONObject();
        for (CanalEntry.Column column : columns) {
            json.put(column.getName(), column.getValue());
        }
        for (CanalEntry.Column column : columns) {
            if (PRIMARY_KEY.equalsIgnoreCase(column.getName())) {
                String key = tableName + SEPARATOR + column.getValue();
                redisUtils.set(key, json.toJSONString());
                log.info("redis数据同步新增，key：" + key);
                break;
            }
        }
    }

    private void redisUpdate(String tableName, List<CanalEntry.Column> columns) {
        JSONObject json = new JSONObject();
        for (CanalEntry.Column column : columns) {
            json.put(column.getName(), column.getValue());
        }
        for (CanalEntry.Column column : columns) {
            if (PRIMARY_KEY.equalsIgnoreCase(column.getName())) {
                String key = tableName + SEPARATOR + column.getValue();
                redisUtils.set(key, json.toJSONString());
                log.info("redis数据同步更新，key：" + key);
                break;
            }
        }
    }

    private void redisDelete(String tableName, List<CanalEntry.Column> columns) {
        for (CanalEntry.Column column : columns) {
            if (PRIMARY_KEY.equalsIgnoreCase(column.getName())) {
                String key = tableName + SEPARATOR + column.getValue();
                redisUtils.remove(key);
                log.info("redis数据同步删除，key：" + key);
                break;
            }
        }
    }

    private void printColumn(List<CanalEntry.Column> columns) {
        for (CanalEntry.Column column : columns) {
            log.info(column.getName() + " : " + column.getValue() + "    update=" + column.getUpdated());
        }
    }

}