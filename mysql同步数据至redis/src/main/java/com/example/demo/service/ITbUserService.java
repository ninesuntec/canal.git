package com.example.demo.service;

import com.example.demo.po.TbUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author astupidcoder
 * @since 2023-04-06
 */
public interface ITbUserService extends IService<TbUser> {

}
